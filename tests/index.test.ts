import fs = require("fs-extra");
import gulp = require("gulp");
import path = require("path");
import * as tape from "tape";
import through = require("through2");
import { localesBundler, localesDiff } from "../lib";

const bundle = (
  out: string,
  flatten: boolean,
  minify: boolean,
  callback: () => void
) => {
  const build = path.join(__dirname, path.join(out, "locales"));

  if (fs.existsSync(build)) {
    fs.removeSync(build);
  }

  gulp
    .src(path.join(__dirname, path.join("locales", "**", "*.json")))
    .pipe(localesBundler({ flatten, minify }))
    .pipe(gulp.dest(build))
    .pipe(
      through.obj(
        (chunk, enc, callback) => {
          callback(undefined, chunk);
        },
        () => callback()
      )
    );
};

tape("locales bundler flatten and minify", (assert: tape.Test) => {
  bundle("build/flatten_minify", true, true, assert.end);
});
tape("locales bundler minify", (assert: tape.Test) => {
  bundle("build/minify", false, true, assert.end);
});
tape("locales bundler flatten", (assert: tape.Test) => {
  bundle("build/flatten", true, false, assert.end);
});
tape("locales bundler", (assert: tape.Test) => {
  bundle("build/unflatten_unminify", false, false, assert.end);
});

tape("locales diff", (assert: tape.Test) => {
  gulp
    .src(path.join(__dirname, path.join("locales", "**", "*.json")))
    .pipe(localesDiff({ locale: "en" }))
    .pipe(
      through.obj(
        (chunk, enc, callback) => {
          console.log(chunk);
          callback();
        },
        () => assert.end()
      )
    );
});
